#!/usr/bin/env bash

# Exit on any error
set -e

# Set function to be called on exit
trap 'on_exit $?' EXIT

###############################################################################
# on_exit is called at exit, if there was an error it logs an error.
# Globals: none
# Parameters:
# - $1: Script exit code
###############################################################################
on_exit() {
  # Log an error
  if [ "$1" != "0" ]; then
    echo "ERROR: Error $1 occured"
  else
    echo "SUCCESS: Playbook finished successfully"
  fi
  exit $1
}

# Get root ansible dir
ans_dir=$(dirname "$0")/..

# Execute the playbook
ansible-playbook --inventory $ans_dir/../ansible-demo-private/inventory/hosts $ans_dir/playbooks/show-hosts-os-version.yml